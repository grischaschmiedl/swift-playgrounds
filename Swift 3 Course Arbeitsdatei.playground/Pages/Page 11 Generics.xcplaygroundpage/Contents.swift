//: [Previous](@previous)

import Foundation

// Function mit generic Parameter

//folgende Funktion tauscht die Elemente eines 2er-Tupels von Ints aus
func swapInts(_ pixel: (Int, Int)) -> (Int, Int) {
    //let (x,y) = pixel
    return (pixel.1, pixel.0)
}

let newPixel = swapInts((1,2))
print(newPixel)

//und jetzt mit einem beliebigen Typ







// Klassen mit Generic Types





//Optionals erklärt: ein Optional ist ein generic enum!!!
let a: Int? = 12
let b: Optional<Int> = 12
let c: Int? = Optional.some(12)
let d: Int? = Optional.none

//: [Next](@next)
