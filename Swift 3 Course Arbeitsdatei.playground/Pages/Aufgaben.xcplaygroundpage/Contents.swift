//: [Previous](@previous)

import Foundation

/* Aufgabe 1
 a) deklariere eine veränderbare optionale Variable "Pixel" vom Typ Tupel zum Speichern der RGB-Values als Int eines Pixels. Die einzelnen Values sollen ebenfalls optionals sein.
 b) initialisiere das Pixel auf den verschiedene Werte, z.B. 1,2,3 oder 1,nil,nil ....
 c) überprüfe mit if let, ob das Pixel ein Optional ist dann und ob die 3 Ints Optionals sind und wenn nicht, dann wandle die Werte in Double um und gib sie mit Print so aus:
 "R: 1.0, G: 2.0, B: 3.0"
 */







/* Aufgabe 2
 a) Definiere eine FUnktion CalculateArithmeticSum:From:To, welche Ints als EIngabeparameter bekommt und die Summe der aritmetischen Reihe (von ersten Param bis zweiten Param) zurückliefert.
 b) Rufe die Funktion auf und gib das Ergebnis mir print aus.
 c) Schreibe die Funktion um, nenne sie CalculateArithmeticSum, die beiden Params sollen unbenannt sein und vom Typ Int?, die Funktion muss also die Parameter prüfen (if let oder guard) und soll  auch Int? zurückliefern
 d) wie b)
 */





/* Aufgabe 3
 a) erzeuge ein 2-dimensionales Array von Ints (= ein Array, welches wieder Arrays enthält)
 Fülle dieses Array zuerst mit 100 Nullen
 b) fülle das Array mit den Werten Zeile * Spalte (also eine Multiplikationstabelle von 1*1 bis 10*10
 c) Gib den Inhalt der elle aus, in der 5*9 gerechnet wurde
 */





/* Aufgabe 4
 a) definiere ein enum für unterschiedliche Studienrichtungen (BMT, DMT, BIS,...)
 b) definiere eine Klasse Person mit den Properties Name: String und Alter:Int?
 c) leite von Person zwei Subklassen ab:
 Angestellter (mit zusätzlichen Gehalt:Int?) und
 Student (mit zusätzlichen Property Studienrichtung: das enum aus a)
 d) erstelle ein Array aus Personen
 e) lege ein paar Personen unterschiedlicher Subklassen und leg sie im Array ab.
 f) schreibe eine Funktion, die ein Dict des Typs wie oben nimmt und die Anzahl aller STUDENTEN zurückliefert, die "Medientechnik" studieren. Rufe die Funktion auf.
 
 g) verwende die Array.sorted-Funktion um das Array absteigen alphabetisch nach dem Alter zu sortieren. Ist das Alter nil, so soll die person am Ende des Arrays eingeordnet werden.
 */




/* Aufgabe 9
 a) definiere eine Klasse Person und eine Klasse Kreditkarte
 Person hat die Properties Name: String und karte: Kreditkarte
 Kreditkarte hat die Properties owner: Person und KKNumber: String
 b) erzeuge eine Person mit einer Kreditkarte
 c) setze die person auf nil und prüfe, ob die Kreditkarte auch released wird.
 */



//: [Next](@next)
