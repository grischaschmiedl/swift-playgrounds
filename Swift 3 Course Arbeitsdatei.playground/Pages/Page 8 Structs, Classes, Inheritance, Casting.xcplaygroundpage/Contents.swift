//: [Previous](@previous)

import Foundation

enum Autotyp {
    case LKW(gesamtgewicht: Int)
    case Sportwagen(ps: Int, farbe: String)
    case Familienkutsche(AnzSitze: Int)
    case Unbekannt
}

//Structures:



//Klassen: kein Autokonstruktor - entweder Konstruktor, oder Defaultwerte oder Optionals!


//Änderung der Properties geht auch wenn die referenz mit "let" deklariert wurde


//Vererbung


//: CASTING
// Teste Typ mit "is" und force-downcast mit as! (wenn Du sicher bist)


//optional downcast, returns nil, if type is incompatible




// "?." ... Aufruf des Property oder Methode wenn nicht nil




// Method Overloading auch mit komplett gleichen Übergabevariablentypen

//: [Next](@next)
