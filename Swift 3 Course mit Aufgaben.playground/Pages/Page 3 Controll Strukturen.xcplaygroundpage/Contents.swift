//: [Previous](@previous)

import Foundation

// for in - Loop
for i in 0..<100 {
    var value = sin(M_PI*4/100*Double(i))
    //print("Wert: \(value)")
}

//while
var anz = 0
var sum = 0
while anz < 100 {
    anz+=1
    sum += anz
}

//repeat while (Fibonacci-Zahlen)
var a = 1
var b = 1
var c: Int
repeat {
    c = a+b
    a = b
    b = c
} while b < 10000

//: [Next](@next)
