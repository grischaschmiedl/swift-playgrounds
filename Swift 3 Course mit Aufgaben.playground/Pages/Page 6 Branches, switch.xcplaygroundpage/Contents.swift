//: [Previous](@previous)

import Foundation

// if bzw if let

// guard

// switch

let alter = 45
switch alter {
    case 0...9:
        print("Kind")
    case 10..<20:
        print("Teen")
    case 20,21,22,23,24,25,26,27,28,29:
        print("Twen")
    case 30...40:
        print("BiVi - Bis Vierzig")
    default:
        print("UHU - unter Hundert")
}


// Zuweisung im Case bei Tuppels
let koord = (12,0)
switch koord {
    case (0, let y):
        print("auf x-Achse und y ist \(y)")
    case (let x, 0):
        print("auf y-Achse und x ist \(x)")
    default:
        print("iorgendwo")
}


//: [Next](@next)
