//: [Previous](@previous)

import Foundation

enum Autotyp {
    case LKW(gesamtgewicht: Int)
    case Sportwagen(ps: Int, farbe: String)
    case Familienkutsche(AnzSitze: Int)
    case Unbekannt
}

//Structures und Classes:
struct Auto {
    var marke: String
    var typ: Autotyp
    var autonummer: String
    func say() {
        print("Das ist ein Auto der Marke \(self.marke)")
    }
}

let grischasAuto = Auto(marke: "Skoda", typ: .Familienkutsche(AnzSitze: 5), autonummer: "WU-999AX")

grischasAuto.say()
// das geht nicht, da der Struct mit let definiert wurde:
// Ändere oben auf var, dann geht es
//grischasAuto.autonummer = "geheim"

var petersAuto = grischasAuto //macht eine Kopie
petersAuto.marke = "VW"
grischasAuto.say()


//Klassen: kein Autokonstruktor - entweder Konstruktor, oder Defaultwerte oder Optionals!
class cAuto {
    var marke = ""
    var typ = Autotyp.Unbekannt
    var autonummer = ""
    func say() {
        print("Das ist ein Auto der Marke \(self.marke)")
    }
}

let grischasZweitesAuto = cAuto()
//Änderung der Properties geht da nur die referenz ist "let"
grischasZweitesAuto.marke = "Ferrari"
grischasZweitesAuto.typ = .Sportwagen(ps: 500, farbe: "rot")
grischasZweitesAuto.autonummer = "WU-XXX"


// **************************************************************************************************

//Vererbung
class Medium {
    var name: String
    init(name: String) {
        self.name = name
    }
}

class Book: Medium {
    var author: String
    init(name: String, author: String) {
        self.author = author
        super.init(name: name)
    }
}

class Song: Medium {
    var artist: String
    init(name: String, artist: String){
        self.artist = artist
        super.init(name: name)
    }
}

//teste den Typ von library mit Alt-Klick
let library = [
    Book(name: "Harry Potter", author: "C.K. Rowling"),
    Song(name: "Satisfaction", artist: "Rolling Stones"),
    Book(name: "The Hobbit", author: "wos was i")
]

for item in library {
    print(item.name)
    // kein Zugriff auf die anderen Properties, da Item = Medium
}


//: CASTING
// Teste Typ mit "is" und force-downcast mit as! (wenn Du sicher bist)
if library[0] is Book {
    let firstBook = library[0] as! Book
    print("\(firstBook.name) by \(firstBook.author)")
}

//optional downcast, returns nil, if type is incompatible
if let firstBook = library[0] as? Book {
    print("\(firstBook.name) by \(firstBook.author)")
}


// Objekte sind reference types
var oneBook: Book?
oneBook = library[0] as? Book
var secondBook = oneBook
oneBook?.author = "James"
secondBook?.author



// "?." ... Aufruf des Property oder Methode wenn nicht nil
class PairOfBooks {
    var firstBook: Book?
    var secondBook: Book?
}
var myPair: PairOfBooks? //optional ... nil
myPair = PairOfBooks()   //optional ... objekt
var thePair = myPair!    //objekt ausgepackt (kein optional mehr)
thePair.firstBook = Book(name: "Pipi Langstrumpf", author:"Astrid Lindgren")

myPair = thePair //myPair ist immer noch ein optional

myPair?.firstBook?.author

var selfUnwrappingPair: PairOfBooks!
selfUnwrappingPair = thePair //wenn diese Zeile auskommentiert wird - Fehler!
selfUnwrappingPair.firstBook?.author



// Method Overloading auch mit komplett gleichen Übergabevariablentypen
public class test {
    public func say(_ number: Int, aWord a: String ) {
        print(a)
    }
    public func say(_ number: Int, anotherWord a: String ) {
        print(a+a+a+a)
    }
}
let x = test()
x.say(3, aWord: "Hallo")
x.say(3, anotherWord: "Hallo")

//: [Next](@next)
