//: [Previous](@previous)

import Foundation

//Arrays mit bestimmten Typ:
var a: Array<String> = Array<String>()
a.append("Hallo")
let b = Array<String>(repeating: "x", count: 5)
let c = ["noch eine Art"]
let d = a + b + c
print(c)

//Arrays mit any Typ
var specialArray: Array<Any> = Array<Any>()
specialArray.append("Hallo")
specialArray.append(12)

var anyItem = specialArray[0]
var strItem = specialArray[0] as! String

specialArray.append("someValue")
specialArray
var lastItem = specialArray.popLast()
specialArray
specialArray.insert("firstElement", at: 0)
specialArray

//Array durchlaufen
var strAll: String = ""
for oneItem in specialArray {
    strAll += "\(oneItem) "
}
strAll

for var i in 1...specialArray.count {
    
}


// Sets = Array mit eindeutigen Werten
var set1: Set<String> = Set<String>()
set1.insert("Georg")

// oder:
var set2: Set<String> = ["Georg", "Hannes", "Beate", "Fritz", "Georg"]
set2

//Typ des Sets kann wieder erkannt werden:
var set3: Set = ["Georg", "Hannes", "Beate", "Fritz", "Georg"]
set2



//Dictionary: Zuordnung von einem key (eindeutig) zu value
var dict1 = [Int: String]()
dict1[13] = "Tennis"

var dict2 = [1: "Tennis", 2: "Fußball"]

// iteration:
for (Index, Wert) in dict2 {
    print("\(Index): \(Wert)")
}

//: Gehe zur Aufgabe 3

//: [Next](@next)
