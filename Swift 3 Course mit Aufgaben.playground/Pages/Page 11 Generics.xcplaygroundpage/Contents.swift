//: [Previous](@previous)

import Foundation

// Function mit generic Parameter

//folgende Funktion tauscht die Elemente eines 2er-Tupels von Ints aus
func swapInts(_ pixel: (Int, Int)) -> (Int, Int) {
    //let (x,y) = pixel
    return (pixel.1, pixel.0)
}

let newPixel = swapInts((1,2))
print(newPixel)

//und jetzt mit einem beliebigen Typ
func swap<T>(_ element: (T,T)) -> (T,T) {
    return (element.1, element.0)
}

print(swap((2,3)))
print(swap(("ele 1","ele 2")))
//aber das geht nicht: 
//swap(1,"hj")


// Klassen mit Generic Types

class Stack<T> {
  public var liste: [T] = []
    public func push(_ ele:T) {
        self.liste.append(ele)
    }
    public func pop() -> T {
        return self.liste.removeLast()
    }
    //Funktion soll eine Summe aller Elemente als Int zurückgeben
    //aber wie addiert man ein Int zu einem unbekannten Typ????
    //der User muss Closure übergeben, welches die Addition durchführt
    public func Sum(_ nullElement: T,_ Add: (T,T)-> T) -> T {
        var s = nullElement
        for ele in liste {
            s = Add(s,ele)
        }
        return s
    }
}

let intStack: Stack<Int> = Stack()
intStack.push(4)
intStack.push(5)
intStack.push(6)
let x = intStack.Sum(0) {(a:Int, b:Int) in a+b}
print(x)

let tupleStack: Stack<(Int,Int)> = Stack()
tupleStack.push((1,2))
tupleStack.push((3,4))
let tupleSum = tupleStack.Sum((0,0))
    {(a: (Int,Int),b: (Int,Int)) -> (Int, Int) in
        return (a.0+b.0,a.1+b.1)
    }

print(tupleSum)



//Optionals erklärt: ein Optional ist ein generic enum!!!
let a: Int? = 12
let b: Optional<Int> = 12
let c: Int? = Optional.some(12)
let d: Int? = Optional.none

//: [Next](@next)
