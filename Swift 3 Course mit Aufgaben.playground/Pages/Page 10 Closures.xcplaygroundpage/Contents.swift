//: [Previous](@previous)

import Foundation

var c: () -> String //Deklaration einer Closure
c = { return "Hello"}
print(c())

// Deklaration
var c2: (String, Int) -> String
// Zuweisung einer Closure
c2 = {(param1, param2) in return "\(param1): \(param2)"}
//Ausführung
c2("Hallo", 12)



//Sorting an array with the sort function
var essen = ["Schnitzel", "Spaghetti", "Berner", "Frankfurter"]
var orderedEssen = essen.sorted()

// Variante mit Closure
orderedEssen = essen.sorted(by: {(s1: String, s2:String) -> Bool in
    return s1 > s2})
orderedEssen

// Varinate Closure ausserhalb der Klammer (immer wenn letzter Parameter)
orderedEssen = essen.sorted {(s1: String, s2:String) -> Bool in
    return s1 > s2}
orderedEssen

// oder kürzer: Typ implizit durch Deklaration erkannt
orderedEssen = essen.sorted(by: {s1, s2 in return s1 > s2})
orderedEssen

// oder kürzer: bei Einzeiler implizit "return"
orderedEssen = essen.sorted(by: {s1,s2 in s1 > s2})
orderedEssen

// oder kürzer: implizite Parameternamen $0, $1, ....
orderedEssen = essen.sorted(by: {$0 > $1})
orderedEssen

// oder wieder ohne Klammer:
orderedEssen = essen.sorted {$0 > $1}
orderedEssen


//: [Next](@next)
