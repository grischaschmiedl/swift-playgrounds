//: [Previous](@previous)

import Foundation

//Strings
var str: String //Deklaration
str = "Won't you buy me a " //Zuweisung

let auto = "Mercedes Benz" //implizite Deklaration + Zuweisung
let songtext = str + auto
let neu = "mein \(auto) ist toll"

let tag = true //booolean
let dblZahl = 12.0/7 //double
let floatZahl: Float = 12/7 //float

//Tuppel
var Pixel = (12, 14, "grün")
//macht einen Error: Pixel = (1,1,1)
let (x,y,pixFarbe) = Pixel
pixFarbe

let farbe2 = Pixel.2



let strZahl = "10"

// Optionals: Int oder nil
var intZahl: Int? 

print("intZahl = \(intZahl)")

// Teste ob nil: geht aber nicht elegant!
intZahl = Int(strZahl)
if intZahl == nil {
    print("intZahl ist nil")
} else {
    print("intZahl ist nicht nil")
}

//schöner: "if let" - Kombination
if let intZahl = Int(strZahl) {
    print("\(intZahl)")
} else{
    print("intZahl ist nil")
}

//implizit unwrapped optional - so sollte man es nicht machen
var theZahl: Int!
theZahl = Int("12")
if theZahl == nil {
    print("war wohl nix")
} else {
    theZahl
}

//Nil-Coalescing:
let neueZahl: Int = intZahl ?? 99


// Fragezeichen für bedingte Methodenausführung (optional chaining)
var intZahl3 = Int(strZahl)
intZahl3?.bigEndian





//: Gehe zur Aufgabe 1



//: [Next](@next)
