//: [Previous](@previous)

import Foundation

//deklaration einer Enumeration
enum Kompasskurs {
    case Nord
    case Süd
    case Ost
    case West
}

var aktuellerKurs: Kompasskurs
aktuellerKurs = Kompasskurs.Nord
aktuellerKurs = .Ost //nur wenn schon deklariert


enum Autotyp {
    case LKW(gesamtgewicht: Int)
    case Sportwagen(ps: Int, farbe: String)
    case Familienkutsche(AnzSitze: Int)
    case Unbekannt
}

let meinAuto = Autotyp.Sportwagen(ps: 500, farbe: "rot")
switch meinAuto {
case .LKW(let gewicht):
    print(gewicht)
case .Sportwagen(let ps, let f):
   print("Yeah, ein \(f)er Sportwagen mit \(ps) PS")
default:
    print("a scho wurscht")
}





//: [Next](@next)
