//: [Previous](@previous)

import Foundation

class listItem {
    var value: String
    var succ: listItem?
    weak var pred: listItem?
    init(withValue value: String) {self.value = value}
    deinit {
        print("Objekt mit Value = \(self.value) wurde released")
    }
    public func add(successor item: listItem) {
        if self.succ == nil {
            self.succ = item
            //das folgende Statemnet macht eine doppelt verkette Liste,
            //die nicht mehr released wird - außer man verwendet weak!
            self.succ?.pred = self
        } else {
            self.succ?.add(successor: item)
        }
    }
}

var myList: listItem?
myList = listItem(withValue: "first");
myList?.add(successor: listItem(withValue: "second"))
myList?.add(successor: listItem(withValue: "third"))
myList //schau was drin ist - 3 verkette Glieder

myList = nil

//: [Next](@next)
