//: [Previous](@previous)

import Foundation

/* Aufgabe 1
 a) deklariere eine veränderbare optionale Variable "Pixel" vom Typ Tupel zum Speichern der RGB-Values als Int eines Pixels. Die einzelnen Values sollen ebenfalls optionals sein.
 b) initialisiere das Pixel auf den verschiedene Werte, z.B. 1,2,3 oder 1,nil,nil ....
 c) überprüfe mit if let, ob das Pixel ein Optional ist dann und ob die 3 Ints Optionals sind und wenn nicht, dann wandle die Werte in Double um und gib sie mit Print so aus:
 "R: 1.0, G: 2.0, B: 3.0"
 */

var Pixel: (Int?, Int?, Int?)?
Pixel = (1,2,5)
if let (r,g,b) = Pixel {
    if let rr=r, let gg=g, let bb=b {
        print("R: \(Double(rr)), G: \(Double(gg)), B: \(Double(bb))")
    }
}
// beachte Double(Int) -> Double
// aber:   Double(String) -> Double?




/* Aufgabe 2
 a) Definiere eine FUnktion CalculateArithmeticSum:From:To, welche Ints als EIngabeparameter bekommt und die Summe der aritmetischen Reihe (von ersten Param bis zweiten Param) zurückliefert.
 b) Rufe die Funktion auf und gib das Ergebnis mir print aus.
 c) Schreibe die Funktion um, nenne sie CalculateArithmeticSum, die beiden Params sollen unbenannt sein und vom Typ Int?, die Funktion muss also die Parameter prüfen (if let oder guard) und soll  auch Int? zurückliefern
 d) wie b)
 */

func CalculateArithmeticSum(from von: Int, to bis: Int) -> Int {
    var summe = 0
    for index in von...bis {
        summe+=index
    }
    return summe
}
print(CalculateArithmeticSum(from: 1, to: 100))

func CalculateArithmeticSum2(_ von: Int?, _ bis: Int?) -> Int? {
    guard let a=von, let b=bis else {return nil}
    var summe = 0
    for index in a...b {
        summe+=index
    }
    return summe
}
print(CalculateArithmeticSum2(1, 100)!)
//Beachte: obwohl die Varibale summe kein Option ist, wird ein Int? zurückgeliefert, da dies in der >Funktion so deklariert ist.


/* Aufgabe 3
 a) erzeuge ein 2-dimensionales Array von Ints (= ein Array, welches wieder Arrays enthält)
 Fülle dieses Array zuerst mit 100 Nullen
 b) fülle das Array mit den Werten Zeile * Spalte (also eine Multiplikationstabelle von 1*1 bis 10*10
 c) Gib den Inhalt der elle aus, in der 5*9 gerechnet wurde
 */

var table: [[Int]] = Array(repeating: Array(repeating: 0, count: 10), count: 10)
for i in 0..<10 {
    for j in 0..<10 {
        table[i][j] = (i+1)*(j+1)
    }
}
table[4][8]
table

/* Aufgabe 4
 a) definiere ein enum für unterschiedliche Studienrichtungen (BMT, DMT, BIS,...)
 b) definiere eine Klasse Person mit den Properties Name: String und Alter:Int?
 c) leite von Person zwei Subklassen ab:
 Angestellter (mit zusätzlichen Gehalt:Int?) und
 Student (mit zusätzlichen Property Studienrichtung: das enum aus a)
 d) erstelle ein Array aus Personen
 e) lege ein paar Personen unterschiedlicher Subklassen und leg sie im Array ab.
 f) schreibe eine Funktion, die ein Dict des Typs wie oben nimmt und die Anzahl aller STUDENTEN zurückliefert, die "Medientechnik" studieren. Rufe die Funktion auf.
 
 g) verwende die Array.sorted-Funktion um das Array absteigen alphabetisch nach dem Alter zu sortieren. Ist das Alter nil, so soll die person am Ende des Arrays eingeordnet werden.
 */

//4a:
enum Studienrichtung {
    case BMT
    case DMT
    case BIS
    case BMM
    case whoKnows // brtauche ich, da die Studierichtung bei den Studierenden nicht optional ist
}

// 4b:
class Person {
    var name: String
    var alter: Int?
    init(name: String) {
        self.name = name
    }
}

// 4c:
class Angestellter: Person {
    var Gehalt: Int?
    override init(name: String) {
        super.init(name: name)
    }
}


class Studierender: Person {
    var studienrichtung: Studienrichtung
    override init(name: String) {
        self.studienrichtung = .whoKnows
        super.init(name: name)
    }
    init(name:String, studienrichtung:Studienrichtung) {
        self.studienrichtung = studienrichtung
        super.init(name: name)
    }
}

// 4e:
var persons = [Angestellter(name: "Grischa"),
               Studierender(name: "Peter", studienrichtung: .BMT),
               Studierender(name: "Paul", studienrichtung: .DMT),
               Studierender(name: "Franz", studienrichtung: .DMT)
]


// 4f:
func getNumberOfBmtStudents(fromArray: [Person]) -> Int {
    var summe = 0
    for p in fromArray {
        if let studi = p as? Studierender {
            if studi.studienrichtung == .BMT {
                summe+=1
            }
        }
    }
    return summe
}

getNumberOfBmtStudents(fromArray: persons)


// 4g:

persons[0].alter = 45
persons[2].alter = 21
persons[3].alter = 23

persons.sorted {(p1:Person, p2:Person) in
    guard let a1 = p1.alter else {return false}
    guard let a2 = p2.alter else {return true}
    return a1 > a2
}

for pers in persons {
    print("\(pers.name) : \(pers.alter)")
}

/* Aufgabe 9
 a) definiere eine Klasse Person und eine Klasse Kreditkarte
 Person hat die Properties Name: String und karte: Kreditkarte
 Kreditkarte hat die Properties owner: Person und KKNumber: String
 b) erzeuge eine Person mit einer Kreditkarte
 c) setze die person auf nil und prüfe, ob die Kreditkarte auch released wird.
 */

class Person2 {
    public let name: String
    public var karte: Kreditkarte?
    init(_ name: String) {self.name = name}
}
class Kreditkarte {
    public weak var owner: Person2?
    public let KKNumber: String
    init(_ KKNumber: String) {self.KKNumber = KKNumber}
    deinit{print("KK released")}
}

var harry: Person2? = Person2("Harry")
harry!.karte = Kreditkarte("dfghjhgfd")
harry = nil


//: [Next](@next)
