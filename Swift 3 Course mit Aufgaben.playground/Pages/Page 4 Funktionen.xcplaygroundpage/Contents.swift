//: [Previous](@previous)

import Foundation

//Standard: //alle Parameterb haben einen Namen, der beim AUfruf angegeben werden muss
//compatibel zu Objective-C
//sprich "Add A to B and C" bzw. AddA:toB:andC
func Add(A a: Int, toB b: Int, AndC c:Int) -> Int {
    return a+b+c
}
var res = Add(A:12, toB: 23, AndC:1)
//C-Sharp-Schreibweise: (SumA:12 toB:23 andC:1)


// ersten Parameter auch schreiben, 2. Paramname = Variablenname
func Sum(A: Int, B: Int, C: Int) -> Int {
    return A+B+C
}
res = Sum(A:12, B:12, C:32)


//alle Parameternamen auslassen
func SwiftSum(_ A: Int, _ B: Int, _ C: Int) -> Int {
    return A+B+C
}
res = SwiftSum(12, 12, 32)

// Problem: nested if let - Statements
func DivideOptionals(_ a:Int?, _ b:Int?) -> Int? {
    if let aa = a {
        if let bb = b {
            if bb != 0 {
                return aa/bb
            } else {
                return nil;
            }
        } else {
            return nil
        }
    } else {
        return nil
    }
}
DivideOptionals(12, 7)

// mit guard let ... umgekehrtes if let
func DivideOptionals2(_ a:Int?, _ b:Int?) -> Double? {
    //guard let aa = a else {return nil}
    //guard let bb = b else {return nil}
    guard let aa=a, let bb = b else {return nil}
    if bb == 0 {return nil}
    return Double(aa)/Double(bb)
}
DivideOptionals2(12,7)

//: Gehe zur Aufgabe 2

//: [Next](@next)
